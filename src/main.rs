use attheme::{Attheme, Color, ColorSignature::Hex, VARIABLES};
use dotenv::dotenv;
use rand::{distributions::Alphanumeric, prelude::*};
use std::{env, sync::Arc};
use tbot::{
    contexts::Unhandled,
    types::{input_file::Document, update},
    Bot,
};

async fn unhandled(context: Arc<Unhandled>) {
    let message = match &context.update {
        update::Kind::Message(message) => message,
        _ => return,
    };

    let (theme, name) = {
        let mut random = rand::thread_rng();
        let mut theme = Attheme::with_capacity(VARIABLES.len());

        for variable in VARIABLES {
            let (red, green, blue) = random.gen();
            theme.variables.insert(
                variable.to_string(),
                Color::new(red, green, blue, 255),
            );
        }

        let name_length = random.gen_range(1, 21);
        let name: String =
            random.sample_iter(&Alphanumeric).take(name_length).collect();
        let name = format!("{}.attheme", name);

        (theme, name)
    };
    let bytes = theme.to_bytes(Hex);
    let document = Document::with_bytes(&name, &bytes);

    let call_result = context
        .bot
        .send_document(message.chat.id, document)
        .in_reply_to(message.id)
        .call()
        .await;

    if let Err(err) = call_result {
        dbg!(err);
    }
}

#[tokio::main]
async fn main() {
    let _ = dotenv();
    let mut bot = Bot::from_env("BOT_TOKEN").event_loop();

    bot.unhandled(unhandled);

    if let Ok(url) = env::var("WEBHOOK_URL") {
        let port = env::var("WEBHOOK_PORT")
            .ok()
            .and_then(|x| x.parse().ok())
            .expect("Specify WEBHOOK_PORT");
        bot.webhook(&url, port).http().start().await.unwrap();
    } else {
        bot.polling().start().await.unwrap();
    }
}
